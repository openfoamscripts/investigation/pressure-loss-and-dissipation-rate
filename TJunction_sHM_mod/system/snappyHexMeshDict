/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  v2212                                 |
|   \\  /    A nd           | Website:  www.openfoam.com                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    object      snappyHexMeshDict;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

// Which of the steps to run
castellatedMesh true;
snap            true;
addLayers       false;


// Geometry. Definition of all surfaces. All surfaces are of class
// searchableSurface.
// Surfaces are used
// - to specify refinement for any mesh cell intersecting it
// - to specify refinement for any mesh cell inside/outside/near
// - to 'snap' the mesh boundary to the surface
geometry
{
    _inlet.stl
    {
        type triSurfaceMesh;
        name inlet;
    }
    _outlet1.stl
    {
        type triSurfaceMesh;
        name outlet1;
    }
    _outlet2.stl
    {
        type triSurfaceMesh;
        name outlet2;
    }
    _front.stl
    {
        type triSurfaceMesh;
        name front;
    }
    _back.stl
    {
        type triSurfaceMesh;
        name back;
    }
    _wall.stl
    {
        type triSurfaceMesh;
        name wall;
    }

    refinementBox
    {
        type box;
        min  (0.198 -0.012 0.00);
        max  (0.210  0.012 0.02);
    }
}


castellatedMeshControls
{
    maxLocalCells 100000;
    maxGlobalCells 2000000;
    minRefinementCells 10;
    maxLoadUnbalance 0.10;
    nCellsBetweenLevels 3;

    features
    (
        {
            file "_inlet.eMesh";
            level 0;
        }
        {
            file "_outlet1.eMesh";
            level 0;
        }
        {
            file "_outlet2.eMesh";
            level 0;
        }
        {
            file "_front.eMesh";
            level 0;
        }
        {
            file "_back.eMesh";
            level 0;
        }
        {
            file "_wall.eMesh";
            level 0;
        }
    );

    refinementSurfaces
    {
        inlet
        {
            level (0 0);
            patchInfo
            {
                type patch;
            }
        }
        "(outlet1|outlet2)"
        {
            level (0 0);
            patchInfo
            {
                type patch;
            }
        }
        "(front|back)"
        {
            level (0 0);
            patchInfo
            {
                type empty;
            }
        }
        wall
        {
            level (0 0);
            patchInfo
            {
                type wall;
            }
        }
    }

    resolveFeatureAngle 30;

    refinementRegions
    {
        // refinementBox
        // {
        //     mode inside;
        //     levels      ((1E15 0));
        //     levelIncrement  (2 2 (2 2 0));
        // }
    }

    locationInMesh (0.2 0 0.01);

    allowFreeStandingZoneFaces false;
}


snapControls
{
    nSmoothPatch 5;
    nSmoothInternal 5;
    tolerance 5.0;
    nSolveIter 30;
    nRelaxIter 5;
    nFeatureSnapIter 10;
    // nFaceSplitInterval 1;
    implicitFeatureSnap false;
    explicitFeatureSnap true;
    multiRegionFeatureSnap false;
    // releasePoints       true;
    // nFaceSplitInterval  10;
    // avoidDiagonal       true;
    // strictRegionSnap    true;
}


addLayersControls
{
    relativeSizes true;

    layers
    {
        wall
        {
            nSurfaceLayers 10;
        }
    }

    expansionRatio 1.2;

    finalLayerThickness 0.8;

    minThickness 1e-6;

    nGrow 0;

    featureAngle 120;

    slipFeatureAngle 60;

    nRelaxIter 3;

    // Number of smoothing iterations of surface normals
    nSmoothSurfaceNormals 1;

    // Number of smoothing iterations of interior mesh movement direction
    nSmoothNormals 3;

    // Smooth layer thickness over surface patches
    nSmoothThickness 10;

    // Stop layer growth on highly warped cells
    maxFaceThicknessRatio 0.5;

    // Reduce layer growth where ratio thickness to medial
    // distance is large
    maxThicknessToMedialRatio 0.3;

    // Angle used to pick up medial axis points
    // Note: changed(corrected) w.r.t 1.7.x! 90 degrees corresponds to 130
    // in 1.7.x.
    minMedialAxisAngle 90;


    // Create buffer region for new layer terminations
    nBufferCellsNoExtrude 0;


    // Overall max number of layer addition iterations. The mesher will exit
    // if it reaches this number of iterations; possibly with an illegal
    // mesh.
    nLayerIter 50;
}



// Generic mesh quality settings. At any undoable phase these determine
// where to undo.
meshQualityControls
{
    #include "meshQualityDict"


    // Advanced

    //- Number of error distribution iterations
    nSmoothScale 4;
    //- Amount to scale back displacement at error points
    errorReduction 0.75;
}


// Advanced

// Write flags
writeFlags
(
    scalarLevels
    layerSets
    layerFields     // write volScalarField for layer coverage
);


// Merge tolerance. Is fraction of overall bounding box of initial mesh.
// Note: the write tolerance needs to be higher than this.
mergeTolerance 1e-6;


// ************************************************************************* //

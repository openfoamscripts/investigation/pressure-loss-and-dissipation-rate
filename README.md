# 圧力損失と散逸率Φの関係

M. TANAKA

[TOC]

## 1. 背景

## 作業メモ

- stlファイルをmmからmに変換

    ```bash
    ls | grep 'stl' | sed 's/\.stl//' | xargs -I@ surfaceConvert -scale 0.001 @.stl _@.stl
    ``` 

- cfMesh(cartesian2DMesh)のためにfmsファイルの作成

    - 全ての`_xxx.stl`ファイルにおいて，`Mesh`を`xxx`に書き換え（手動）

    ```bash 
    cat _inlet.stl _outlet1.stl _outlet2.stl _wall.stl > _TJunction.stl
    surfaceFeatureEdges -angle 0 _TJunction.stl _TJunction.fms
    ``` 

    - pressure loss: -1.769247e-01
    - dissipation  : 5.172801e-02

- Reの定義
    - 代表長さLは流路幅とする．L = 0.02 m
    - 代表速度Uは出口側管での平均速度（入口速度の半分）とする．
    - 動粘度は 1.5e-05 m^2/s (空気を想定)

- U=100 (Re = 67,000)
    - セル細かめ，Rなし
        - pressure loss: -1.222954e+02
        - dissipation  : 2.761284e+01
        - 比: 0.226

    - セル細かめ，Rあり
        - pressure loss: -9.280326e+01
        - dissipation  : 3.561063e+01
        - 比: 0.384

    - あり/なし
        - pressure loss: 0.759
        - dissipation  : 1.290


- U=10 (Re = 6,700)
    - セル細かめ，Rなし
        - pressure loss: -1.552530e-01
        - dissipation  : 8.358382e-02
        - 比: 0.538

    - セル細かめ，Rあり
        - pressure loss: -1.346569e-01
        - dissipation  : 8.276555e-02
        - 比: 0.615

    - あり/なし
        - pressure loss: 0.867
        - dissipation  : 0.990


- U=1 (Re = 670)
    - セル細かめ，Rなし
        - pressure loss: -2.449524e-04
        - dissipation  : 1.372986e-04
        - 比: 0.561

    - セル細かめ，Rあり
        - pressure loss: -2.795564e-04
        - dissipation  : 1.565355e-04
        - 比: 0.560

    - あり/なし
        - pressure loss: 1.141
        - dissipation  : 1.140


- U=0.1 (Re = 67)
    - セル細かめ，Rなし
        - pressure loss: -8.385945e-07
        - dissipation  : 7.072698e-07
        - 比: 0.843

    - セル細かめ，Rあり
        - pressure loss: -1.047095e-06
        - dissipation  : 9.132027e-07
        - 比: 0.872

    - あり/なし
        - pressure loss: 1.249
        - dissipation  : 1.291


- Re下げると流路を滑らかにしたことよりも，流路幅を狭めたことが効いてくる？
    - 滑らかにした方が圧損が大きい．．．
    - 逆に流速上げてみる？

    - Re = w * U / nu
        - 実際のストッカだと？
        - w = 0.005 m, U = 1 m/s, nu = 1.5*10^-5 m^2/s (窒素でなく空気だが) と概算すると，Re = U * w / nu ≈ 330．
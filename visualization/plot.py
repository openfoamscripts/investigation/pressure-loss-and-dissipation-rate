import numpy as np
import matplotlib.pyplot as plt


data = np.loadtxt(
    'data.csv',   # 読み込みたいファイルのパス
    delimiter=',',      # ファイルの区切り文字
    skiprows=1,         # 先頭の何行を無視するか（指定した行数までは読み込まない）
    usecols=(0,1)     # 読み込みたい列番号
)

Re = data[:, 0]
pT_ratio = data[:, 1]

fig = plt.figure(figsize=(5, 5))
ax = fig.add_subplot(111)

tcf = ax.plot(Re, pT_ratio, 'D--r')

ax.set_title('Effect of Pipe Smoothing')
ax.set_xlabel('Reynold\'s Number [-]')
ax.set_ylabel('Reduction Ratio of Pressure Loss [-]')
ax.semilogx()
# ax.set_xlim(0, 140)
# ax.set_ylim(0, 9)


plt.grid()
plt.savefig('reductionOfPressureLossViaReynoldsNumber.png')
plt.show()
plt.close()